﻿using UnityEngine;
using UnityEditor;
using System;
using System.IO;
  

public class AssetImportSettings : AssetPostprocessor
{

    public string materialPathnew = PasstroughScript.materialPathnew;


    public void OnPreprocessModel()
    {
        if (PasstroughScript.materialPathnew == null)
        {
            PasstroughScript.materialPathnew = "Assets/ArtAssets/Materials/";
        }



        string hourMinute = DateTime.Now.ToString("HH:mm");
        string hoursandminutes = "\t" + hourMinute + "\t";
        var importer = assetImporter as ModelImporter;
        var fileNameIndex = assetPath.LastIndexOf('/');
        var fileName = assetPath.Substring(fileNameIndex + 1);
        if (!fileName.Contains("_")) return;

        importer.materialLocation = ModelImporterMaterialLocation.External;
        importer.importMaterials = false;

        importer.materialSearch = ModelImporterMaterialSearch.Everywhere;
        importer.materialName = ModelImporterMaterialName.BasedOnMaterialName;

        //createing The materials and strings for that purpose

        string ProjectPath = Application.dataPath.Substring(0, Application.dataPath.Length - 6);
       

      //  string materialFolderPath = "Assets/ArtAssets/Materials"; ;

        string[] split = assetPath.Split('/');
        string category = "";
        string categoryWhole = "";


        for (int i = 0; i < split.Length - 2; i++)
        {
            category += split[i] + "/";
        }

        for (int e = 0; e < split.Length - 1; e++)
        {
            categoryWhole += split[e] + "/";
        }
        int removenumb = category.Length;
        string catergoryFolderslashed = categoryWhole.Remove(categoryWhole.LastIndexOf(categoryWhole), removenumb );
        string catergoryFolderName = catergoryFolderslashed.Remove(catergoryFolderslashed.LastIndexOf('/'));
        string materialpath = materialPathnew + categoryWhole.Remove(categoryWhole.LastIndexOf(categoryWhole),removenumb);

        //create var for type of material

        var material = new Material(Shader.Find("Standard (Roughness setup)"));

        //create material if path exist
      
        string  Folderpath = materialPathnew + catergoryFolderName;
       

        string MaterialPath = ProjectPath + Folderpath + "/" + Selection.activeObject.name + ".mat";
        
        if (File.Exists(MaterialPath) == false)
         {

            if (AssetDatabase.IsValidFolder(Folderpath))
            {
                AssetDatabase.CreateAsset(material, materialpath + Selection.activeObject.name + ".mat");
                Debug.Log("Material Addded at - "+ materialpath + hoursandminutes);
                OnPostprocessMaterial(material);
            }
            else // create path and material to store it 
            {
                AssetDatabase.CreateFolder(materialPathnew.Substring(0, materialPathnew.Length - 1), catergoryFolderName);
                Debug.Log("Folder and material Created  at - " + materialpath + "\t" + hoursandminutes);
                AssetDatabase.CreateAsset(material, materialpath + Selection.activeObject.name + ".mat");
                OnPostprocessMaterial(material);
            } //materialPathnew.Remove(materialPathnew.Length, 1)
        }
        else
        {
            Debug.Log("Material already exists"+ materialpath +"\t" + hoursandminutes);
        }
        


    }



    // moet nog getriggered worden wanneer ik m material assign aan modol 
     void OnPostprocessMaterial(Material material)
    {  
        // naming of the textures
        int fileNameIndex = assetPath.LastIndexOf('/');
        string fileName = assetPath.Substring(fileNameIndex + 1);
        string matchNameDif = fileName.Substring(0, fileName.Length - 4) + "_D";
        string matchNameNor = fileName.Substring(0, fileName.Length - 4) + "_N";
        string matchNameRoug = fileName.Substring(0, fileName.Length - 4) + "_R";
        string matchNameMet = fileName.Substring(0, fileName.Length - 4) + "_M";
        string[] guids = AssetDatabase.FindAssets("t:Texture");

        // map checks
        bool foundTexturesDif = false;
        bool foundTexturesNor = false;
        bool foundTexturesRoug = false;
        bool foundTexturesMet = false;

        // look for the textures in an array
        foreach (string guid in guids)
        {
            string path = AssetDatabase.GUIDToAssetPath(guid);
            if (path.Contains(matchNameDif))
            {
                material.mainTexture = AssetDatabase.LoadAssetAtPath<Texture>(path);
                foundTexturesDif = true;
            }
            

            if (path.Contains(matchNameNor))
            {
                material.SetTexture("_BumpMap", AssetDatabase.LoadAssetAtPath<Texture>(path));
                foundTexturesNor = true;
            }

            if (path.Contains(matchNameRoug))
            {
                material.SetTexture("_SpecGlossMap", AssetDatabase.LoadAssetAtPath<Texture>(path));
                foundTexturesRoug = true;
            }
 

            if (path.Contains(matchNameMet))
            {
                material.SetTexture("_MetallicGlossMap", AssetDatabase.LoadAssetAtPath<Texture>(path));
                foundTexturesMet = true;
            }

        

        }
      
               // checking if textures are found if not show message for artist
               if (!foundTexturesDif)
               {
                  Debug.Log("Diffuse Map Missing");
               }

               if (!foundTexturesNor)
               {
                  Debug.Log("Normal Map Missing");
               }

               if (!foundTexturesRoug)
               {
                  Debug.Log(" Diffuse Map Missing");  
               }

           if (!foundTexturesMet)
               {
                  Debug.Log(" Metallic Map Missing");
               }
               
    }


}



    // Hicham Ouchan Technical Artist - assetimporter script - project sci-fi horror

