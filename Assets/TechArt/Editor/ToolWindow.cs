﻿using UnityEngine;
using UnityEditor;
using System.Collections;


public class UnityWindowCustom : EditorWindow
{
#pragma warning disable 1557
    // Creates the window and namespace 
    string fieldinput = "";
    Texture2D headerTexture;
    Texture2D headerBackgroundTexture;

    Color headerBackgroundColor = new Color(56/255f, 88/255f, 115/255f, 100);

    Rect headerTextureSection, headerBackgroundSection, TripleButton1, TripleButton2, TripleButton3;

    

    [MenuItem("Window/Lika Tools")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow<UnityWindowCustom>("Lika Tools");
        var window = EditorWindow.GetWindow<UnityWindowCustom>();
        window.minSize = new Vector2(200, 200);
        
        



    }

    void DrawCoords()
    {
     

        headerBackgroundSection.x = 0;
        headerBackgroundSection.y = 0;
        headerBackgroundSection.width = position.width;
        headerBackgroundSection.height = 80;
        
        headerTextureSection = headerTextureSection.CenterOnX(position.width / 2);
        headerTextureSection.width = 150;
        headerTextureSection.height = 80;

        TripleButton1.x = 0;
        TripleButton1.y = 167;
        TripleButton1.width = ((position.width / 2) + 50);
        TripleButton1.height = 20;

        TripleButton2.x = 0;
        TripleButton2.y = 190;
        TripleButton2.width = ((position.width / 2) + 50);
        TripleButton2.height = 30;

        TripleButton3.x = ((position.width / 3)*2);
        TripleButton3.y = TripleButton1.y;
        TripleButton3.width = (position.width / 3) - 10;
        TripleButton3.height = TripleButton1.height;
    }
    
    void InitTextures()
    {
        headerTexture = Resources.Load<Texture2D>("Icons/Editors/HeaderIcon");
        headerBackgroundTexture = new Texture2D(1, 1);
        headerBackgroundTexture.SetPixel(0, 0, headerBackgroundColor);
        headerBackgroundTexture.Apply();
    }
    void OnGUI() // Tells what to draw in the window
    {
        
        InitTextures();
        DrawCoords();
        //header
        GUI.DrawTexture(headerBackgroundSection, headerBackgroundTexture);
        GUI.DrawTexture(headerTextureSection, headerTexture);
        GUILayoutUtility.GetRect(headerTextureSection.width, headerTextureSection.height);
        GUILayout.Label("Random Colors!", EditorStyles.boldLabel);
        if (GUILayout.Button("Generate!"));
        {
            GenerateColor();
        }

        if (GUILayout.Button("Reset"))
        {
            ResetColor();
        }

        //OherTools
        GUILayout.Label("Asset Import Settings", EditorStyles.boldLabel);

        //Three Butt
     
        fieldinput = EditorGUILayout.TextField("Material Root Folder", fieldinput,GUILayout.Width(TripleButton1.width));

      

        if (GUI.Button(TripleButton3, "Save"))
        {
            fieldinput = fieldinput.Replace(@"\", "/");
            PasstroughScript.materialPathnew = fieldinput;
            Debug.Log("Material Root Folder assigned to  " + fieldinput);
        }
        GUI.Label(TripleButton2, "Current Material Root Folder:   " +  PasstroughScript.materialPathnew);


     }


    void GenerateColor() //void for color generating
    {
        foreach (GameObject obj in Selection.gameObjects)
        {
            Renderer renderer = obj.GetComponent<Renderer>();
            if (renderer != null)
            {
                renderer.material.color = Random.ColorHSV();
            }
        }
    }
    
    void CheckMetal() // checking if the metallic has aplha and setting it to none in import settings
    {

       // foreach (GameObject Sel in Selection.gameObjects)
        {

        }
    }
    
    void ResetColor() //reset color 
    {
        foreach (GameObject obj in Selection.gameObjects)
        {
            Renderer renderer = obj.GetComponent<Renderer>();

            {
                renderer.material.color = Color.white;
            }
        }
    }
}
