﻿using UnityEngine;

public static class RectExtensions
{
    public static Rect CenterOnX(this Rect r, float x)
    {
        r.xMin = x - (r.width / 2);
        return r;
    }
}