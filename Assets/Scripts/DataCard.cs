﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class DataCard : MonoBehaviour {

    [SerializeField]
    private uint dataLength;
    [SerializeField]
    private string initialData;
    private byte[] data;

	public bool writeData(byte[] newData)
    {
        if (newData.Length != dataLength)
        {
            return false;
        }

        for (int i = 0; i < dataLength; i++)
        {
            data[i] = newData[i];
        }

        return true;
    }

    public byte[] readData()
    {
        byte[] newData = new byte[dataLength];
        for (int i = 0; i < dataLength; i++)
        {
            newData[i] = data[i];
        }

        return newData;
    }

    public void clearData()
    {
        if (data == null) { data = new byte[dataLength]; }
        for (int i = 0; i < dataLength; i++)
        {
            data[i] = 0;
        }
    }

    private void Start()
    {
        clearData();

        if (initialData != "")
        {
            byte[] newData = new byte[dataLength];
            byte[] strData = Encoding.ASCII.GetBytes(initialData);

            if (strData.Length > dataLength)
            {
                Debug.LogWarning("Initial data too large: " + strData.Length + " > " + dataLength);
            }
            else
            {
                for (int i = 0; i < strData.Length; i++)
                {
                    newData[i] = strData[i];
                }

                writeData(newData);
            }
        }
    }
}
