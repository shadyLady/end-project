﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.AI; 

public class NavmeshToMesh : MonoBehaviour
{
	[MenuItem("GameObject/Convert Navmesh to Mesh")]
	static void ConvertNavMesh()
	{
		NavMeshTriangulation nmt = NavMesh.CalculateTriangulation();

		Mesh mesh = new Mesh();
		mesh.vertices = nmt.vertices;
		mesh.triangles = nmt.indices;

		AssetDatabase.CreateAsset(mesh, "Assets/NavigationMesh.asset"); 
	}
}
