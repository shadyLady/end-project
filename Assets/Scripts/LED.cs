﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LED : MonoBehaviour {

    private Renderer _renderer;

    private void _setColor(Color c)
    {
        _renderer.material.color = c;
    }

    public void setColor(Color c)
    {
        _setColor(c);
    }

    public void setColor(float r, float g, float b)
    {
        _setColor(new Color(r, g, b));
    }

	void Start () {
        _renderer = GetComponent<Renderer>();
        _setColor(new Color(0f, 0f, 0f));
	}
}
