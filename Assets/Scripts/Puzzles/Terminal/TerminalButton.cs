﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System; 

public class TerminalButton : MonoBehaviour
{
	public Action ButtonPressed;

	[SerializeField]
	private List<TerminalLight> _terminalLights; 

	public void PressButton()
	{
		foreach (TerminalLight light in _terminalLights)
			light.Toggle(); 

		if (ButtonPressed != null)
			ButtonPressed(); 
	}
}
