﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class TerminalController : MonoBehaviour
{
	[SerializeField]
	private List<TerminalLight> _terminalLights;

	[SerializeField]
	private List<TerminalButton> _terminalButtons;

	[SerializeField]
	private AnimatedDoor _terminalDoor; 

	[SerializeField]
	private TeleportPoint _cleaningRoomTeleportPoint; 

	private void Awake()
	{
		foreach (TerminalButton button in _terminalButtons)
			button.ButtonPressed += OnButtonPressed; 
	}

	private void OnButtonPressed()
	{
		foreach (TerminalLight light in _terminalLights)
		{
			if (!light.active)
				return; 
		}

		Debug.Log("All lights activated"); 

		foreach (TerminalButton button in _terminalButtons)
			button.ButtonPressed -= OnButtonPressed;

		_terminalDoor.OpenDoor(); 

		_cleaningRoomTeleportPoint.SetLocked(false); 
	}
}
