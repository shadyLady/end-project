﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System; 

public class TerminalLight : MonoBehaviour
{
	public bool active { get; private set; }

	[SerializeField] 
	private Color _activeColor;

	[SerializeField] 
	private Color _inactiveColor; 

	private MeshRenderer _meshRenderer;

	private void Awake()
	{
		_meshRenderer = GetComponent<MeshRenderer>();
	}

	public void Toggle()
	{
		active = !active;

		if (!active)
			_meshRenderer.material.color = _inactiveColor;
		else _meshRenderer.material.color = _activeColor; 
	}
}
