﻿using System.Collections;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class CleaningRoom : MonoBehaviour
{
	[SerializeField]
	private Door[] _entreeDoors;

	[SerializeField]
	private Door[] _exitDoors;

	[SerializeField]
	private AnimatedDoor _entreeDoor;

	[SerializeField]
	private AnimatedDoor _exitDoor; 

	[SerializeField]
	private float _processDuration = 1.0f;

	[SerializeField]
	private TeleportPoint _targetTeleportPoint;

	[SerializeField]
	private TeleportPoint _hallwayPoint;

	private ParticleSystem[] _cleaningSystems; 

	private void Awake()
	{
		_cleaningSystems = GetComponentsInChildren<ParticleSystem>(); 

		if (_targetTeleportPoint != null)
			_targetTeleportPoint.TeleportCall += InitiateCleaningProcess; 
	}

	public void InitiateCleaningProcess()
	{
		StartCoroutine(CleaningProcess()); 
	}

	private IEnumerator CleaningProcess()
	{
		_entreeDoor.CloseDoor(); 

		foreach (ParticleSystem system in _cleaningSystems)
			system.Play();

		yield return new WaitForSeconds(_processDuration);

		_hallwayPoint.SetLocked(false);

		foreach (ParticleSystem system in _cleaningSystems)
			system.Stop();

		_entreeDoor.OpenDoor();

		_exitDoor.OpenDoor(); 
	}

	private void OnDestroy()
	{
		if (_targetTeleportPoint != null)
			_targetTeleportPoint.TeleportCall -= InitiateCleaningProcess; 
	}
}
