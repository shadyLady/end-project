﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGamePopUp : MonoBehaviour
{
	[SerializeField]
	private GameManager _gameManager; 

	private void Awake()
	{
		_gameManager.OnQuitGame += Open;
		Close();
	}

	private void Open()
	{
		gameObject.SetActive(true); 
	}

	public void Close()
	{
		gameObject.SetActive(false); 
	}

	private void OnDestroy()
	{
		_gameManager.OnQuitGame -= Open; 
	}
}
