﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshToTpArea : MonoBehaviour
{
    private void Start()
    {
        NavMeshTriangulation data = NavMesh.CalculateTriangulation();

        Mesh newMesh = new Mesh();

        newMesh.vertices = data.vertices;
        newMesh.triangles = data.indices;

        gameObject.GetComponent<MeshFilter>().mesh = newMesh;
        gameObject.GetComponent<MeshCollider>().sharedMesh = newMesh;
    }
}
