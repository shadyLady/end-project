﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class RenderModel : MonoBehaviour
{
	private Hand _parentHand;

	private Hand.HandType _currentHandType;

	private void Start()
	{
		_parentHand = GetComponentInParent<Hand>();

		_currentHandType = _parentHand.GuessCurrentHandType();

		if (_currentHandType == Hand.HandType.Left)
			FlipModel(); 
	}

	private void FlipModel()
	{
		Vector3 currentScale = transform.localScale;

		currentScale.x *= -1;
		currentScale.y *= -1;
		currentScale.z *= -1;

		transform.localScale = currentScale;

		Vector3 euler = transform.eulerAngles;

		euler.y = 180; 
		euler.z = 180;

		transform.eulerAngles = euler; 
	}
}
