﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem; 

public class AutomaticDoor : MonoBehaviour
{
	[SerializeField]
	private TeleportPoint _triggerTeleport;

	[SerializeField]
	private AnimatedDoor _animatedDoor; 
	
	private void Awake()
	{
		if (_triggerTeleport != null)
			_triggerTeleport.TeleportCall += OnTeleportToTrigger; 
	}

	private void OnTeleportToTrigger()
	{
		_animatedDoor.OpenDoor(); 
	}

	private void OnDisable()
	{
		if (_triggerTeleport != null)
			_triggerTeleport.TeleportCall -= OnTeleportToTrigger; 
	}
}
