﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class Screw : MonoBehaviour
{
	public delegate void ScrewEvent(Screw screw);
	public ScrewEvent ScrewDetached; 

	[SerializeField]
	private Transform _screwDriverTip;

	[SerializeField]
	private Transform _rotationParent;

	private Quaternion _lastQuaternion;

	private float _lastAngle; 

	private float _angle;

	private float _totalRotation;

	//To get screw out of wall 
	private float _requiredRotation = 720;

	[SerializeField]
	private float _screwLength = 0.3f;

	private Vector3 _startPosition;

	private void Awake()
	{
		 _startPosition = transform.position;
	}

	private void Update()
	{
		if (_screwDriverTip == null || _rotationParent == null)
			return;

		//Rotating along with screw driver
		float currentAngle = _rotationParent.transform.eulerAngles.z;
		Quaternion rotation = _screwDriverTip.rotation;

		_angle = Quaternion.Angle(rotation, _lastQuaternion);

		if (currentAngle <= _lastAngle)
			_angle *= -1;

		transform.RotateAround(transform.position, transform.forward, _angle);

		_lastQuaternion = rotation;
		_lastAngle = currentAngle;

		//Displacing, according to rotation
		if (_totalRotation >= _requiredRotation)
			return;

		_totalRotation += _angle;

		if (Mathf.Abs(_totalRotation) < 0) _totalRotation = 0; 

		transform.position = _startPosition - transform.forward * (_screwLength * (_totalRotation / _requiredRotation));

		//Check if screw has already been detached
		if (_totalRotation >= _requiredRotation)
		{
			if (ScrewDetached != null)
				ScrewDetached(this);

			Rigidbody rb = GetComponent<Rigidbody>();
			rb.useGravity = true;
		}
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag != "ScrewDriver")
			return;

		_screwDriverTip = other.transform;

		Hand hand = _screwDriverTip.GetComponentInParent<Hand>();

		if (hand == null)
			return; 

		_rotationParent = hand.transform;

		_lastQuaternion = _screwDriverTip.rotation;
		_lastAngle = 0f;
	}

	private void OnTriggerExit(Collider other)
	{
		if (_screwDriverTip != null)
			_screwDriverTip = null; 
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(_startPosition - (transform.forward * _screwLength), 0.2f); 
	}
}
