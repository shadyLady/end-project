﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class AnimatedDoor : MonoBehaviour
{
	private Animator _animator;

	private AudioSource _audioSource;

	[SerializeField]
	private AudioClip _audioClip;

	private void Awake()
	{
		_animator = GetComponent<Animator>();

		if (_audioClip != null)
		{
			_audioSource = gameObject.AddComponent<AudioSource>();
			_audioSource.clip = _audioClip; 
		}
	}

	public void OpenDoor()
	{
		_animator.SetBool("Open", true);

		if (_audioClip != null)
			_audioSource.Play(); 
	}

	public void CloseDoor()
	{
		_animator.SetBool("Open", false);

		if (_audioClip != null)
			_audioSource.Play();
	}
}
