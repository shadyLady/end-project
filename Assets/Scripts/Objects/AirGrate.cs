﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirGrate : MonoBehaviour
{
	private Screw[] _screws;

	private int _detachedCount; 

	private void Awake()
	{
		_screws = GetComponentsInChildren<Screw>();

		foreach (Screw screw in _screws)
		{
			screw.ScrewDetached += OnScrewDetached; 
		}
	}

	private void OnScrewDetached(Screw screw)
	{
		screw.ScrewDetached -= OnScrewDetached;

		_detachedCount++;

		if (_detachedCount >= _screws.Length)
		{
			GetComponent<Rigidbody>().useGravity = true;
		}
	}
}
