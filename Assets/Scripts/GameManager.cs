﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public System.Action OnQuitGame; 

	public void QuitGameClicked()
	{
		if (OnQuitGame != null)
			OnQuitGame(); 
	}

	public void QuitGameConfirmed()
	{
		Application.Quit(); 
	}
}
