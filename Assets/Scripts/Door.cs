﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

    [SerializeField] private float timeItTakes;
    [SerializeField] private AnimationCurve AnimCurve;
    [SerializeField] private Transform StartPoint;
    [SerializeField] private Transform EndPoint;

    private float posValue = 0;
    private float dir = 0;

    public void Open()
    {
        dir = 1;
    }

    public void Close()
    {
        dir = -1;
    }

    public void Stop()
    {
        dir = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (dir == 1 && posValue < 1)
        {
            posValue += Time.deltaTime / timeItTakes;
        }
        if (dir == -1 && posValue > 0)
        {
            posValue -= Time.deltaTime / timeItTakes;
        }

        float animval = AnimCurve.Evaluate(posValue);
        transform.position = Vector3.Lerp(StartPoint.position, EndPoint.position, animval);
        transform.rotation = Quaternion.Lerp(StartPoint.rotation, EndPoint.rotation, animval);
	}
}
