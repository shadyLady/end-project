﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScreenCardReader : MonoBehaviour {

    [SerializeField] TMPro.TextMeshProUGUI screen;
    [SerializeField] CardReader reader;

	void Start () {
        reader.onSuccess.AddListener(new UnityEngine.Events.UnityAction(cardSuccess));
        reader.onFail.AddListener(new UnityEngine.Events.UnityAction(cardFail));
        reader.onReset.AddListener(new UnityEngine.Events.UnityAction(resetScreen));

        resetScreen();
    }

    private void cardSuccess()
    {
        screen.color = new Color(0f, 1f, 0f);
        screen.SetText("Access Granted");
    }

    private void cardFail()
    {
        screen.color = new Color(1f, 0f, 0f);
        screen.SetText("Access Denied");
    }

    public void resetScreen()
    {
        screen.color = new Color(1f, 1f, 1f);
        screen.SetText("Awaiting Access Card");
    }
}
