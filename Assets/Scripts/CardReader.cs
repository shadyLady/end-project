﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class CardReader : MonoBehaviour {

    [Header("This will be converted to bytes")]
    public string accessKey;

    private float _timerAmount = 3f;
    private float _timer = 0f;
    private bool _didReset = true;

    private byte[] _accessKey;

    public UnityEvent onSuccess;
    public UnityEvent onFail;
    public UnityEvent onReset;

    [SerializeField] private LED powerLED;
    [SerializeField] private LED statusLED;

    private Queue<DataCard> _cardQueue;

	private void Start() {
        Debug.Log("Starting card reader...");
        _accessKey = Encoding.ASCII.GetBytes(accessKey);

        powerLED.setColor(0f, 1f, 0f);

        _cardQueue = new Queue<DataCard>();
	}

    private void OnTriggerEnter(Collider other)
    {
        DataCard c = other.GetComponent<DataCard>();
        if (c != null)
            _cardQueue.Enqueue(c);
    }

    private void Update()
    {
        if (_timer < 0f)
        {
            if (_didReset == false)
            {
                statusLED.setColor(0f, 0f, 0f);
                onReset.Invoke();
                _didReset = true;
            }

            if (_cardQueue.Count > 0)
            {
                _didReset = false;
                _timer = _timerAmount;
                statusLED.setColor(1f, 0.5f, 0f);

                DataCard c = _cardQueue.Dequeue();
                byte[] data = c.readData();

                if (data.Length < _accessKey.Length)
                {
                    statusLED.setColor(1f, 0f, 0f);
                    onFail.Invoke();
                }
                else
                {
                    bool stillGood = true;
                    for (int i = 0; i < _accessKey.Length; i++)
                    {
                        if (data[i] != _accessKey[i])
                        {
                            stillGood = false;
                        }
                    }

                    if (stillGood == true)
                    {
                        statusLED.setColor(0f, 1f, 0f);
                        onSuccess.Invoke();
                    }
                    else
                    {
                        statusLED.setColor(1f, 0f, 0f);
                        onFail.Invoke();
                    }
                }
            }
        }
        else
        {
            _timer -= Time.deltaTime;
        }
    }
}
